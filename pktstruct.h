#ifndef PKTSTRUCT_H
#define PKTSTRUCT_H
#include <stdint.h>

typedef struct {
    uint8_t dest_mac [6];
    uint8_t send_mac [6];
    uint16_t eth_type;
} __attribute__((packed)) eth_header ;

typedef struct {
    uint8_t version;
    uint8_t type;
    uint16_t total_length;
    uint16_t identification;
    uint16_t fragment;
    uint8_t ttl;
    uint8_t protocol;
    uint16_t chksum;
    uint8_t src_addr[4];
    uint8_t dst_addr[4];
} __attribute__((packed)) ip_header ;

typedef struct {
    uint16_t src_port;
    uint16_t dst_port;
    uint32_t seq_num;
    uint8_t D_off;
    uint8_t flag;
    uint16_t win_size;
    uint16_t chksum;
    uint16_t URG;
} __attribute__((packed)) tcp_header ;

typedef struct {
    uint16_t src_port;
    uint16_t dst_port;
    uint16_t length;
    uint16_t chksum;
} __attribute__((packed)) udp_header ;
#endif // PKTSTRUCT_H
