#include <pcap.h>
#include <pktstruct.h>
#include <stdio.h>

int check_eth(eth_header* eth_h){
    printf("DST_MAC:\t%02X:%02X:%02X:%02X:%02X:%02X\n",eth_h->dest_mac[0],eth_h->dest_mac[1],eth_h->dest_mac[2],eth_h->dest_mac[3],eth_h->dest_mac[4],eth_h->dest_mac[5]);
    printf("SND_MAC:\t%02X:%02X:%02X:%02X:%02X:%02X\n",eth_h->send_mac[0],eth_h->send_mac[1],eth_h->send_mac[2],eth_h->send_mac[3],eth_h->send_mac[4],eth_h->send_mac[5]);
    printf("NEXT_FRAME:\t");
    switch (eth_h->eth_type) {
        case 0x0008:
            printf("Internet Protocol version 4 (IPv4) \n");
        break;
        case 0x0608:
            printf("Address Resolution Protocol (ARP)  \n");
        break;
        case 0xDD86:
            printf("Internet Protocol Version 6 (IPv6)  \n");
        break;
        default:
            printf("OTHER PACKETS.. \n");
        break;
    }
    return eth_h->eth_type;

}

int check_ip(ip_header * ip_h){
    printf("DST_IP :\t%d.%d.%d.%d\n",ip_h->dst_addr[0],ip_h->dst_addr[1],ip_h->dst_addr[2],ip_h->dst_addr[3]);
    printf("SRC_IP :\t%d.%d.%d.%d\n",ip_h->src_addr[0],ip_h->src_addr[1],ip_h->src_addr[2],ip_h->src_addr[3]);
    printf("NEXT_FRAME:\t");
    switch (ip_h->protocol) {
        case 0x01:
            printf("ICMP \n");
        break;
        case 0x06:
            printf("TCP \n");
        break;
        case 0x11:
            printf("UDP \n");
        break;
        default:
            printf("sOTHER PACKETS.. \n");
        break;
    }
    return ip_h->protocol;
}

int check_tcp(tcp_header * tcp_h){
    printf("DST_PORT:\t%d\n",tcp_h->dst_port);
    printf("SRC_PORT:\t%d\n",tcp_h->src_port);
    return 0;
}

int check_udp(udp_header * udp_h){
    printf("DST_PORT:\t%d\n",udp_h->dst_port);
    printf("SRC_PORT:\t%d\n",udp_h->src_port);
    return 0;
}

void print_data(u_char * data){

//    printf("DATA[10](str):\n");
//    for (int i = 0 ; i < 10 ; i++)
//        printf("%c",data[i]);
//    printf("\n");

    printf("DATA[10](BYTE):\n");
    for (int i = 0 ; i < 10 ; i++)
        printf("%02X ",data[i]);
    printf("\n");
}

void packet_handler(u_char *param, const struct pcap_pkthdr *header, const u_char *pkt_data)
{

    eth_header * eth_h;
    ip_header * ip_h;
    tcp_header * tcp_h;
    udp_header * udp_h;
    u_char * data;
    int flag = 0;

    printf("\n\n--------------------------------------\n");

    /////////////////////////////ETH///////////////////////////////
    eth_h = (eth_header*) pkt_data;
    flag = check_eth(eth_h);
    ///////////////////////////////////////////////////////////////


    ////////////////////////////IP/////////////////////////////////
    if( flag == 0x0008){ //check is ipv4
        ip_h = (ip_header*) (pkt_data + sizeof (eth_header));
        flag = check_ip(ip_h);
    }else {
        return;
    }
    ///////////////////////////////////////////////////////////////



    ///////////////////////////TCP or UDP/////////////////////////////////
    if(flag == 0x06){ //check is tcp
        tcp_h = (tcp_header*) (pkt_data + sizeof (eth_header)+ sizeof (ip_header));
        check_tcp(tcp_h);
        data = (u_char *) (pkt_data + sizeof (eth_header)+ sizeof (ip_header) + sizeof (tcp_header));
        print_data(data);
    }
    else if(flag == 0x11){ //check is udp
        udp_h = (udp_header*) (pkt_data + sizeof (eth_header)+ sizeof (ip_header));
        check_udp(udp_h);
        data = (u_char *) (pkt_data + sizeof (eth_header)+ sizeof (ip_header) + sizeof (udp_header));
        print_data(data);
    }else{
        return;
    }
    //////////////////////////////////////////////////////////////////////

}

int main(int argc, char ** argv)
{
    if(argc < 2){
        printf("Device plz!\n");
    }
    char* dev = argv[1];
    pcap_t * pcapHandle;
    char errbuf[PCAP_ERRBUF_SIZE];
    pcapHandle = pcap_open_live(dev, 65536, 1, 1000, errbuf);
    if(pcapHandle == NULL){
        printf("Handler err\n");
        return -1;
    }
    pcap_loop(pcapHandle, 0, packet_handler, NULL);
    return 0;
}


